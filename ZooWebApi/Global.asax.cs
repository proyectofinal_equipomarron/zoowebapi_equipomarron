using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using ZooWebApi.Services.InyeccionD;

namespace ZooWebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AutofacConfig.ConfigureContainer();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            HttpContext ctx = HttpContext.Current;

            StringBuilder sb = new StringBuilder();
            sb.Append(ctx.Request.Url + Environment.NewLine);
            sb.Append("Source:" + Environment.NewLine + ctx.Server.GetLastError().Source);
            sb.Append("Message:" + Environment.NewLine + ctx.Server.GetLastError().Message);
            sb.Append("Stack Trace:" + Environment.NewLine + ctx.Server.GetLastError().StackTrace);
            var error = sb.ToString();

            string logPath = AppDomain.CurrentDomain.BaseDirectory + "/log.txt";
            using (StreamWriter writer = new StreamWriter(logPath, true))
            {
                writer.WriteLine(DateTime.Now + " : " + error);
            }
        }
    }
}
