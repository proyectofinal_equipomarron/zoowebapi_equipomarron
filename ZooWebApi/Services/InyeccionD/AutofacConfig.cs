﻿using Autofac;
using Autofac.Integration.Mvc;
using System;
using System.Web.Mvc;

namespace ZooWebApi.Services.InyeccionD
{
    /// <summary>
    /// Clase para la configuración de la inyección de dependencias
    /// </summary>
    public class AutofacConfig
    {
        /// <summary>
        /// El contenedor
        /// </summary>
        public static IContainer Container { get; set; }
        /// <summary>
        /// Configurar el Autofac 
        /// </summary>
        public static void ConfigureContainer()
        {
            try
            {
                var builder = new ContainerBuilder();

                // Register dependencies in controllers
                builder.RegisterControllers(typeof(WebApiApplication).Assembly);

                // Register dependencies in filter attributes
                builder.RegisterFilterProvider();

                // Register dependencies in custom views
                builder.RegisterSource(new ViewRegistrationSource());

                //// Register our Data dependencies
                builder.RegisterModule(new GestorDependencias());

                Container = builder.Build();

                // Set MVC DI resolver to use our Autofac container
                DependencyResolver.SetResolver(new AutofacDependencyResolver(Container));
            }
            catch (Exception exc)
            {
                Console.Write(exc.Message);
            }
        }
    }
}