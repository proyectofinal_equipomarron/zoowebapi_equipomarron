﻿using System.Data;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using Autofac;
using ZooWebApi.Controllers;
using ZooWebApi.Services.Repositories;


namespace ZooWebApi.Services.InyeccionD
{
    /// <summary>
    /// Clase para gestionar las inyecciones de dependencias del proyecto
    /// </summary>
    public class GestorDependencias : Module
    {
        private readonly DatabaseRepository db = new DatabaseRepository();
        
        /// <summary>
        /// Carga dependencia
        /// </summary>
        /// <param name="builder">Contenedor</param>
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<AnimalController>();
            bool conexion = CheckConnection();


            if (!conexion)
            {
                builder.RegisterType<FakeRepository>().As<IRepository>().SingleInstance();
            }
            else
            {
                builder.RegisterType<DatabaseRepository>().As<IRepository>().SingleInstance();
            }

            base.Load(builder);
        }

        bool CheckConnection()
        {
            string conString = "Server=tcp:desaprendiendo.database.windows.net,1433;Initial Catalog=Zoo;Persist Security Info=False;User ID=jacintoaisa;Password=P0t@to!!!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30";
            bool isValid = false;
            SqlConnection con = null;
            try
            {
                con = new SqlConnection(conString);
                con.Open();
                isValid = true;

            }
            catch (SqlException ex)
            {
                isValid = false;

            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

            return isValid;
        }
    }
}