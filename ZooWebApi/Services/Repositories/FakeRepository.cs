﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Web;
using ZooWebApi.Models;

namespace ZooWebApi.Services.Repositories
{
    /// <summary>
    /// Repositorio Fake 
    /// Creación de Animales,Animales,Jaulas,Dietas y Alimentos
    /// </summary>
    public class FakeRepository : IRepository
    {
        /// <summary>
        /// Lista de especies
        /// </summary>
        private readonly List<Especie> Especies = new List<Especie>();
        /// <summary>
        /// Lista de jaulas
        /// </summary>
        private readonly List<Jaula> Jaulas = new List<Jaula>();
        /// <summary>
        /// Lista de animales
        /// </summary>
        private readonly List<Animal> Animales = new List<Animal>();
        /// <summary>
        /// Lista de alimentos
        /// </summary>
        private readonly List<Alimento> Alimentos = new List<Alimento>();
        /// <summary>
        /// Lista de dietas
        /// </summary>
        private readonly List<Dieta> Dietas = new List<Dieta>();

        /// <summary>
        /// Constructor del repositorio Fake
        /// 6 animales
        /// 3 especies
        /// 3 jaulas
        /// 6 dietas
        /// 3 alimentos
        /// </summary>
        public FakeRepository()
        {
            Especies.Add(new Especie()
            {
                EspecieID = 1,
                Descripcion = "Mamifero"
            });
            Especies.Add(new Especie()
            {
                EspecieID = 2,
                Descripcion = "Reptil"
            });
            Especies.Add(new Especie()
            {
                EspecieID = 3,
                Descripcion = "Anfibio"
            });

            Jaulas.Add(new Jaula()
            {
                JaulaID = 1,
                PosicionJaula = "Midgar",
                Capacidad = 12
            });
            Jaulas.Add(new Jaula()
            {
                JaulaID = 2,
                PosicionJaula = "Kalm",
                Capacidad = 8
            });
            Jaulas.Add(new Jaula()
            {
                JaulaID = 3,
                PosicionJaula = "Terra",
                Capacidad = 14
            });

            Animales.Add(new Animal()
            {
                AnimalID = 1,
                Nombre = "Egonator",
                FechaNacimiento = DateTime.Now,

                EspecieID = 1,
                JaulaID = 2,
                Especie = Especies.Find(especie => especie.EspecieID == 1),
                Jaula = Jaulas.Find(jaula => jaula.JaulaID == 2)
            });
            Animales.Add(new Animal()
            {
                AnimalID = 2,
                Nombre = "Dragón",
                FechaNacimiento = DateTime.Now,

                EspecieID = 2,
                JaulaID = 2,
                Especie = Especies.Find(especie => especie.EspecieID == 2),
                Jaula = Jaulas.Find(jaula => jaula.JaulaID == 2)
            });
            Animales.Add(new Animal()
            {
                AnimalID = 3,
                Nombre = "Minotauro",
                FechaNacimiento = DateTime.Now,

                EspecieID = 3,
                JaulaID = 1,
                Especie = Especies.Find(especie => especie.EspecieID == 3),
                Jaula = Jaulas.Find(jaula => jaula.JaulaID == 1)
            });
            Animales.Add(new Animal()
            {
                AnimalID = 4,
                Nombre = "Centauro",
                FechaNacimiento = DateTime.Now,

                EspecieID = 3,
                JaulaID = 3,
                Especie = Especies.Find(especie => especie.EspecieID == 3),
                Jaula = Jaulas.Find(jaula => jaula.JaulaID == 3)
            });
            Animales.Add(new Animal()
            {
                AnimalID = 5,
                Nombre = "Hipogrifo",
                FechaNacimiento = DateTime.Now,

                EspecieID = 1,
                JaulaID = 1,
                Especie = Especies.Find(especie => especie.EspecieID == 1),
                Jaula = Jaulas.Find(jaula => jaula.JaulaID == 1)
            });
            Animales.Add(new Animal()
            {
                AnimalID = 6,
                Nombre = "Quimera",
                FechaNacimiento = DateTime.Now,

                EspecieID = 3,
                JaulaID = 2,
                Especie = Especies.Find(especie => especie.EspecieID == 3),
                Jaula = Jaulas.Find(jaula => jaula.JaulaID == 2)
            });
            Alimentos.Add(new Alimento()
            {
                AlimentoID = 1,
                Descripcion = "Pollo",
                Calorias = 500
            });
            Alimentos.Add(new Alimento()
            {
                AlimentoID = 2,
                Descripcion = "Manzanas",
                Calorias = 300
            });
            Alimentos.Add(new Alimento()
            {
                AlimentoID = 3,
                Descripcion = "Zanahorias",
                Calorias = 800
            });
            Dietas.Add(new Dieta()
            {
                ID = 1,
                AnimalID = 1,
                AlimentoID = 3,
                Cantidad = 20
            });
            Dietas.Add(new Dieta()
            {
                ID = 2,
                AnimalID = 2,
                AlimentoID = 1,
                Cantidad = 50
            });
            Dietas.Add(new Dieta()
            {
                ID = 3,
                AnimalID = 3,
                AlimentoID = 2,
                Cantidad = 95
            });
            Dietas.Add(new Dieta()
            {
                ID = 4,
                AnimalID = 4,
                AlimentoID = 2,
                Cantidad = 15
            });
            Dietas.Add(new Dieta()
            {
                ID = 5,
                AnimalID = 5,
                AlimentoID = 2,
                Cantidad = 35
            });
            Dietas.Add(new Dieta()
            {
                ID = 6,
                AnimalID = 6,
                AlimentoID = 2,
                Cantidad = 110
            });
        }
        /// <summary>
        /// Método para pedir la información de un alimento
        /// </summary>
        /// <param name="id">ID del alimento</param>
        /// <returns>El alimento con id igual al parámetro introducido</returns>
        public Alimento DameAlimento(int id)
        {
            var alimentoSeleccionado = from alimento in Alimentos
                                       where alimento.AlimentoID == id
                                       select alimento;
            return alimentoSeleccionado.Any() ? alimentoSeleccionado.First() : null;
        }
        /// <summary>
        /// Método para pedir la información de un animal
        /// </summary>
        /// <param name="id">ID del animal</param>
        /// <returns>El animal con id igual al parámetro introducido</returns>
        public Animal DameAnimal(int id)
        {
            var animalSeleccionado = from animal in Animales
                                     where animal.AnimalID == id
                                     select animal;
            return animalSeleccionado.Any() ? animalSeleccionado.First() : null;
        }
        /// <summary>
        /// Método para pedir la información de una dieta
        /// </summary>
        /// <param name="id">ID de la dieta</param>
        /// <returns>La dieta con id igual al parámetro introducido</returns>
        public Dieta DameDieta(int id)
        {
            var dietaSeleccionado = from dieta in Dietas
                                    where dieta.ID == id
                                    select dieta;
            return dietaSeleccionado.Any() ? dietaSeleccionado.First() : null;
        }
        /// <summary>
        /// Método para pedir la información de una especie
        /// </summary>
        /// <param name="id">ID de la especie</param>
        /// <returns>La especie con id igual al parámetro introducido</returns>
        public Especie DameEspecie(int id)
        {
            var especieSeleccionado = from especie in Especies
                                      where especie.EspecieID == id
                                      select especie;
            return especieSeleccionado.Any() ? especieSeleccionado.First() : null;
        }
        /// <summary>
        /// Método para pedir la información de una jaula
        /// </summary>
        /// <param name="id">ID de la jaula</param>
        /// <returns>La jaula con id igual al parámetro introducido</returns>
        public Jaula DameJaula(int id)
        {
            var jaulaSeleccionado = from jaula in Jaulas
                                    where jaula.JaulaID == id
                                    select jaula;
            return jaulaSeleccionado.Any() ? jaulaSeleccionado.First() : null;
        }
        /// <summary>
        /// Método para pedir la información de todos los alimentos
        /// </summary>
        /// <returns>Lista de todos los alimentos del repositorio</returns>
        public List<Alimento> DameListaAlimentos()
        {
            return Alimentos;
        }
        /// <summary>
        /// Método para pedir la información de todos los animales
        /// </summary>
        /// <returns>Lista de todos los animales del repositorio</returns>
        public List<Animal> DameListaAnimales()
        {
            return Animales;
        }
        /// <summary>
        /// Método para pedir la información de todas las dietas
        /// </summary>
        /// <returns>Lista de todas los dietas del repositorio</returns>
        public List<Dieta> DameListaDietas()
        {
            return Dietas;
        }
        /// <summary>
        /// Método para pedir la información de todas las especies
        /// </summary>
        /// <returns>Lista de todas los especies del repositorio</returns>
        public List<Especie> DameListaEspecie()
        {
            return Especies;
        }
        /// <summary>
        /// Método para pedir la información de todas las jaulas
        /// </summary>
        /// <returns>Lista de todas los jaulas del repositorio</returns>
        public List<Jaula> DameListaJaulas()
        {
            return Jaulas;
        }
        /// <summary>
        /// Creamos animal
        /// </summary>
        /// <param name="animal">Animal que añadimos al repositorio</param>
        public bool CreaAnimal(Animal animal)
        {
            if (animal != null)
            {
                Animales.Add(animal);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Creamos alimento
        /// </summary>
        /// <param name="alimento">Alimento que añadimos al repositorio</param>
        public bool CreaAlimento(Alimento alimento)
        {
            if (alimento != null)
            {
                Alimentos.Add(alimento);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Creamos dieta
        /// </summary>
        /// <param name="dieta">Dieta que añadimos al repositorio</param>
        public bool CreaDieta(Dieta dieta)
        {
            if (dieta != null)
            {
                Dietas.Add(dieta);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Creamos especie
        /// </summary>
        /// <param name="especie">Especie que añadimos al repositorio</param>
        public bool CreaEspecie(Especie especie)
        {
            if (especie != null)
            {
                Especies.Add(especie);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Creamos jaula
        /// </summary>
        /// <param name="jaula">Jaula que añadimos al repositorio</param>
        public bool CreaJaula(Jaula jaula)
        {
            if (jaula != null)
            {
                Jaulas.Add(jaula);
                return true;
            }
            return false;
        }
        /// <summary>
        /// Edita un animal
        /// </summary>
        /// <param name="animal">El animal que va a editar</param>
        /// <returns>true si ha conseguido editarlo</returns>
        public bool EditaAnimal(Animal animal)
        {
            try
            {
                int index = Animales.FindIndex(item => item.AnimalID == animal.AnimalID);
                Animales[index] = animal;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        /// <summary>
        /// Edita un alimento
        /// </summary>
        /// <param name="alimento">El alimento que va a editar</param>
        /// <returns>true si ha conseguido editarlo</returns>
        public bool EditaAlimento(Alimento alimento)
        {
            try
            {
                int index = Alimentos.FindIndex(item => item.AlimentoID == alimento.AlimentoID);
                Alimentos[index] = alimento;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        /// <summary>
        /// Edita un dieta
        /// </summary>
        /// <param name="dieta">La dieta que va a editar</param>
        /// <returns>true si ha conseguido editarlo</returns>
        public bool EditaDieta(Dieta dieta)
        {
            try
            {
                int index = Dietas.FindIndex(item => item.ID == dieta.ID);
                Dietas[index] = dieta;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        /// <summary>
        /// Edita un especie
        /// </summary>
        /// <param name="especie">La especie que va a editar</param>
        /// <returns>true si ha conseguido editarlo</returns>
        public bool EditaEspecie(Especie especie)
        {
            try
            {
                int index = Especies.FindIndex(item => item.EspecieID == especie.EspecieID);
                Especies[index] = especie;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        /// <summary>
        /// Edita un jaula
        /// </summary>
        /// <param name="jaula">La jaula que va a editar</param>
        /// <returns>true si ha conseguido editarlo</returns>
        public bool EditaJaula(Jaula jaula)
        {
            try
            {
                int index = Jaulas.FindIndex(item => item.JaulaID == jaula.JaulaID);
                Jaulas[index] = jaula;
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}