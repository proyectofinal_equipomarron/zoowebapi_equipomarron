﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ZooWebApi.Models;

namespace ZooWebApi.Services.Repositories
{
    /// <summary>
    /// Repositorio a partir de la base de datos
    /// </summary>
    public class DatabaseRepository : IRepository
    {
        readonly ZooEntities _db = new ZooEntities();
        /// <summary>
        /// Método para pedir la información de un alimento
        /// </summary>
        /// <param name="id">ID del alimento</param>
        /// <returns>El alimento con id igual al parámetro introducido</returns>
        public Alimento DameAlimento(int id)
        {
            return _db.Alimento.Find(id);
        }
        /// <summary>
        /// Método para pedir la información de un animal
        /// </summary>
        /// <param name="id">ID del animal</param>
        /// <returns>El animal con id igual al parámetro introducido</returns>
        public Animal DameAnimal(int id)
        {
            return _db.Animal.Find(id);
        }
        /// <summary>
        /// Método para pedir la información de una dieta
        /// </summary>
        /// <param name="id">ID de la dieta</param>
        /// <returns>La dieta con id igual al parámetro introducido</returns>
        public Dieta DameDieta(int id)
        {
            return _db.Dieta.Find(id);
        }
        /// <summary>
        /// Método para pedir la información de una especie
        /// </summary>
        /// <param name="id">ID de la especie</param>
        /// <returns>La especie con id igual al parámetro introducido</returns>
        public Especie DameEspecie(int id)
        {
            return _db.Especie.Find(id);
        }
        /// <summary>
        /// Método para pedir la información de una jaula
        /// </summary>
        /// <param name="id">ID de la jaula</param>
        /// <returns>La jaula con id igual al parámetro introducido</returns>
        public Jaula DameJaula(int id)
        {
            return _db.Jaula.Find(id);
        }
        /// <summary>
        /// Método para pedir la información de todos los alimentos
        /// </summary>
        /// <returns>Lista de todos los alimentos del repositorio</returns>
        public List<Alimento> DameListaAlimentos()
        {
            return _db.Alimento.ToList();
        }
        /// <summary>
        /// Método para pedir la información de todos los animales
        /// </summary>
        /// <returns>Lista de todos los animales del repositorio</returns>
        public List<Animal> DameListaAnimales()
        {
            return _db.Animal.ToList();
        }
        /// <summary>
        /// Método para pedir la información de todas las dietas
        /// </summary>
        /// <returns>Lista de todas los dietas del repositorio</returns>
        public List<Dieta> DameListaDietas()
        {
            return _db.Dieta.ToList();
        }
        /// <summary>
        /// Método para pedir la información de todas las especies
        /// </summary>
        /// <returns>Lista de todas los especies del repositorio</returns>
        public List<Especie> DameListaEspecie()
        {
            return _db.Especie.ToList();
        }
        /// <summary>
        /// Método para pedir la información de todas las jaulas
        /// </summary>
        /// <returns>Lista de todas los jaulas del repositorio</returns>
        public List<Jaula> DameListaJaulas()
        {
            return _db.Jaula.ToList();
        }
        /// <summary>
        /// Método para crear un animal en la base de datos
        /// </summary>
        /// <param name="animal">El animal a crear</param>
        /// <returns>true si lo ha creado correctamente</returns>
        public bool CreaAnimal(Animal animal)
        {
            try
            {
                _db.Animal.Add(animal);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        /// <summary>
        /// Método para crear un alimento en la base de datos
        /// </summary>
        /// <param name="alimento">El alimento a crear</param>
        /// <returns>true si lo ha creado correctamente</returns>
        public bool CreaAlimento(Alimento alimento)
        {
            try
            {
                _db.Alimento.Add(alimento);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        /// <summary>
        /// Método para crear una dieta en la base de datos
        /// </summary>
        /// <param name="dieta">La dieta a crear</param>
        /// <returns>true si lo ha creado correctamente</returns>
        public bool CreaDieta(Dieta dieta)
        {
            try
            {
                _db.Dieta.Add(dieta);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
           
        }
        /// <summary>
        /// Método para crear una especie en la base de datos
        /// </summary>
        /// <param name="especie">La especie a crear</param>
        /// <returns>true si lo ha creado correctamente</returns>
        public bool CreaEspecie(Especie especie)
        {
            try
            {
                _db.Especie.Add(especie);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
        /// <summary>
        /// Método para crear una jaula en la base de datos
        /// </summary>
        /// <param name="jaula">La jaula a crear</param>
        /// <returns>true si lo ha creado correctamente</returns>
        public bool CreaJaula(Jaula jaula)
        {
            try
            {
                _db.Jaula.Add(jaula);
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
            
        }

        public bool EditaAnimal(Animal animal)
        {
            try
            {
                _db.Entry(animal).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool EditaAlimento(Alimento alimento)
        {
            try
            {
                _db.Entry(alimento).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool EditaDieta(Dieta dieta)
        {
            try
            {
                _db.Entry(dieta).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool EditaEspecie(Especie especie)
        {
            try
            {
                _db.Entry(especie).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public bool EditaJaula(Jaula jaula)
        {
            try
            {
                _db.Entry(jaula).State = EntityState.Modified;
                _db.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }
        }
    }
}