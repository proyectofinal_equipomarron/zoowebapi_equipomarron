﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZooWebApi.Models;

namespace ZooWebApi.Services.Repositories
{
    /// <summary>
    /// Interface para crear cualquier repositorio
    /// </summary>
    public interface IRepository
    {
        /* BUSCA POR ID Y TODOS */

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Animal DameAnimal(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Animal> DameListaAnimales();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Alimento DameAlimento(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Alimento> DameListaAlimentos();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Dieta DameDieta(int id);
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Dieta> DameListaDietas();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Especie DameEspecie(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Especie> DameListaEspecie();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Jaula DameJaula(int id);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<Jaula> DameListaJaulas();

        /* CREA ELEMENTO */

        /// <summary>
        /// Crear animal
        /// </summary>
        /// <param name="animal">parametro que creará</param>
        /// <returns>true si ha sido creado correctamente</returns>
        bool CreaAnimal(Animal animal);

        /// <summary>
        /// Crear alimento
        /// </summary>
        /// <param name="alimento">parametro que creará</param>
        /// <returns>true si ha sido creado correctamente</returns>
        bool CreaAlimento(Alimento alimento);

        /// <summary>
        /// Crear dieta
        /// </summary>
        /// <param name="dieta">parametro que creará</param>
        /// <returns>true si ha sido creado correctamente</returns>
        bool CreaDieta(Dieta dieta);

        /// <summary>
        /// Crear especie
        /// </summary>
        /// <param name="especie">parametro que creará</param>
        /// <returns>true si ha sido creado correctamente</returns>
        bool CreaEspecie(Especie especie);

        /// <summary>
        /// Crear jaula
        /// </summary>
        /// <param name="jaula">parametro que creará</param>
        /// <returns>true si ha sido creado correctamente</returns>
        bool CreaJaula(Jaula jaula);

        /* EDITA ELEMENTO */

        /// <summary>
        /// Crear animal
        /// </summary>
        /// <param name="animal">parametro que creará</param>
        /// <returns>true si ha sido creado correctamente</returns>
        bool EditaAnimal(Animal animal);

        /// <summary>
        /// Crear alimento
        /// </summary>
        /// <param name="alimento">parametro que creará</param>
        /// <returns>true si ha sido creado correctamente</returns>
        bool EditaAlimento(Alimento alimento);

        /// <summary>
        /// Crear dieta
        /// </summary>
        /// <param name="dieta">parametro que creará</param>
        /// <returns>true si ha sido creado correctamente</returns>
        bool EditaDieta(Dieta dieta);

        /// <summary>
        /// Crear especie
        /// </summary>
        /// <param name="especie">parametro que creará</param>
        /// <returns>true si ha sido creado correctamente</returns>
        bool EditaEspecie(Especie especie);

        /// <summary>
        /// Crear jaula
        /// </summary>
        /// <param name="jaula">parametro que creará</param>
        /// <returns>true si ha sido creado correctamente</returns>
        bool EditaJaula(Jaula jaula);
    }
}
