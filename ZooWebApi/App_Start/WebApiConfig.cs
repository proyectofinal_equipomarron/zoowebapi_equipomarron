﻿using System.Web.Http;

namespace ZooWebApi
{
    /// <summary>
    /// Configuración del WEbPAPI
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// Registra
        /// </summary>
        /// <param name="config">La configuración</param>
        public static void Register(HttpConfiguration config)
        {
          
            // Configuración y servicios de API web

            // Rutas de API web
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }

            );

            config.Routes.MapHttpRoute(
                name: "DefaultApiWithAction",
                routeTemplate: "Api/{controller}/{action}");
        }
    }
}
