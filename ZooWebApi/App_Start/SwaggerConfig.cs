using System.IO;
using System.Web.Http;
using WebActivatorEx;
using ZooWebApi;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace ZooWebApi
{
    public class SwaggerConfig
    {

        /// <summary>
        /// OBTENEMOS EL PATH DEL ARCHIVO XML DE DOCUMENTACIÓN.
        /// </summary>
        /// <returns>El path</returns>
        protected static string GetXmlCommentsPath()
        {
            return Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, "bin", "ZooWebApi.xml");
        }
        /// <summary>
        /// Registra
        /// </summary>
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
                .EnableSwagger(c =>
                {
                    
                    c.SingleApiVersion("v1", "ZooWebApi")
                        .Description("Wep API para la configuración de un zoo")
                        .TermsOfService("Puedes usar esto siempre y cuando sepas como usarlo")
                        .Contact(x => x
                            .Name("Equipo Marrón")
                            .Url("https://formacion.desaprendiendo.net/campus/")
                            .Email("marron@desaprendiendo.net"))
                        .License(x => x
                            .Name("Licencia")
                            .Url("https://www.google.es/"));

                   
                    c.ApiKey("apiKey")
                        .Description("API Key Authentication")
                        .Name("apiKey")
                        .In("header");
                   
                    c.IncludeXmlComments(GetXmlCommentsPath());

                   
                    c.PrettyPrint();
                })
                .EnableSwaggerUi(c =>
                {
                    
                    c.EnableApiKeySupport("Authorization", "header");
                   
                });
        }
    }
}
