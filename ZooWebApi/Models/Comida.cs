﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ZooWebApi.Models
{
    /// <summary>
    /// Clase auxiliar para la obtención de datos del controlador Alimento, accion AlimentoDemandados
    /// </summary>
    [DataContract]
    public class Comida
    {
        /// <summary>
        /// Nombre del alimento
        /// </summary>
        [DataMemberAttribute]
        public string Alimento { get; set; }
        /// <summary>
        /// Cantidad del alimento
        //Class1.cs/ </summary>
        [DataMemberAttribute]
        public int Cantidad { get; set; }
        /// <summary>
        /// Constructor de la Clase Comida
        /// </summary>
        /// <param name="com">Nombre del alimento</param>
        /// <param name="cant">Cantidad del alimento</param>
        public Comida(string com, int cant)
        {
            Alimento = com;
            Cantidad = cant;
        }
    }
}