﻿using Autofac;
using System.Collections.Generic;
using System.Web.Http;
using ZooWebApi.Models;
using ZooWebApi.Services.InyeccionD;
using ZooWebApi.Services.Repositories;

namespace ZooWebApi.Controllers
{
    /// <summary>
    /// Clase controlador para la clase Dieta
    /// </summary>
    public class DietaController : ApiController
    {
        readonly IRepository _repositorio;
        /// <summary>
        /// Constructor vacio para que realice la inyeccion de dependencias de IRepository
        /// </summary>
        public DietaController()
        {
            using (var scope = AutofacConfig.Container.BeginLifetimeScope())
            {
                _repositorio = scope.Resolve<IRepository>();
            }
        }
        /// <summary>
        /// Contructor con parámetro Irepository para los Test
        /// </summary>
        /// <param name="repositorio">Repositorio Fake para pruebas</param>
        public DietaController(IRepository repositorio)
        {
            _repositorio = repositorio;
        }
        /// <summary>
        /// Método Get para enviar los datos al API
        /// </summary>
        /// <param name="id">Id de Dieta con opción que si no se introduce se pasa nulo</param>
        /// <returns>Lista de Dietas sin parametro o 
        /// Información de la Dieta con el id igual al parámetro</returns>
        [System.Web.Mvc.HttpGet]
        public IHttpActionResult Get(int? id = null)
        {
            if (id == null)
            {
                return Ok(_repositorio.DameListaDietas());
            }
            else
            {
                List<Dieta> dietas = new List<Dieta>
                {
                    _repositorio.DameDieta(id.Value)
                };
                return dietas[0] == null ? (IHttpActionResult)NotFound() : Ok(dietas);
            }
        }
        public IHttpActionResult PostCreaDieta([FromBody] Dieta dieta)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            return _repositorio.CreaDieta(dieta) ? Ok(dieta) : (IHttpActionResult)NotFound();
        }
    }
}