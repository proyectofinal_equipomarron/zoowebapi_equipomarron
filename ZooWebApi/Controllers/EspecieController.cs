﻿using Autofac;
using System.Collections.Generic;
using System.Web.Http;
using ZooWebApi.Models;
using ZooWebApi.Services.InyeccionD;
using ZooWebApi.Services.Repositories;

namespace ZooWebApi.Controllers
{
    /// <summary>
    /// Clase controlador para la clase Especie
    /// </summary>
    public class EspecieController : ApiController
    {
        readonly IRepository _repositorio;
        /// <summary>
        /// Constructor vacio para que realice la inyeccion de dependencias de IRepository
        /// </summary>
        public EspecieController()
        {
            using (var scope = AutofacConfig.Container.BeginLifetimeScope())
            {
                _repositorio = scope.Resolve<IRepository>();
            }
        }
        /// <summary>
        /// Contructor con parámetro Irepository para los Test
        /// </summary>
        /// <param name="repositorio">Repositorio Fake para pruebas</param>
        public EspecieController(IRepository repositorio)
        {
            _repositorio = repositorio;
        }
        /// <summary>
        /// Método Get para enviar los datos al API
        /// </summary>
        /// <param name="id">Id de Especie con opción que si no se introduce se pasa nulo</param>
        /// <returns>Lista de Especie sin parametro o 
        /// Información de la Especie con el id igual al parámetro</returns>
        [System.Web.Mvc.HttpGet]
        public IHttpActionResult Get(int? id = null)
        {
            if (id == null)
            {
                return Ok(_repositorio.DameListaEspecie());
            }
            else
            {
                List<Especie> especies = new List<Especie>
                {
                    _repositorio.DameEspecie(id.Value)
                };
                return especies[0] == null ? (IHttpActionResult)NotFound() : Ok(especies);
            }
        }
        public IHttpActionResult PostCreaEspecie([FromBody] Especie especie)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            return _repositorio.CreaEspecie(especie) ? Ok(especie) : (IHttpActionResult)NotFound();
        }
    }
}
