﻿using Autofac;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ZooWebApi.Models;
using ZooWebApi.Services.InyeccionD;
using ZooWebApi.Services.Repositories;
using RouteAttribute = System.Web.Http.RouteAttribute;

namespace ZooWebApi.Controllers
{
    /// <summary>
    /// Clase controlador para la clase Alimento
    /// </summary>
    public class AlimentoController : ApiController
    {
        /// <summary>
        /// Hola Egon jnebowaognangajnpejg
        /// </summary>
        readonly IRepository _repositorio;
        /// <summary>
        /// Constructor vacio para que realice la inyección de dependencias de IRepository
        /// </summary>
        public AlimentoController()
        {
            using (var scope = AutofacConfig.Container.BeginLifetimeScope())
            {
                _repositorio = scope.Resolve<IRepository>();
            }
        }
        /// <summary>
        /// Constructor con parámetro IRepository para los Test
        /// </summary>
        /// <param name="repositorio">Repositorio Fake para pruebas</param>
        public AlimentoController(IRepository repositorio)
        {
            _repositorio = repositorio;
        }
        /// <summary>
        /// Método Get para enviar los datos al API
        /// </summary>
        /// <param name="id">Id de Alimento con opción que si no se introduce se pasa nulo</param>
        /// <returns>Lista de alimentos sin parametro o 
        /// Información del alimento con el id igual al parámetro</returns>
        [System.Web.Mvc.HttpGet]
        public IHttpActionResult Get(int? id = null)
        {
            if (id == null)
            {
                return Ok(_repositorio.DameListaAlimentos());
            }
            else
            {
                List<Alimento> alimentos = new List<Alimento>
                {
                    _repositorio.DameAlimento(id.Value)
                };
                return alimentos[0] == null ? (IHttpActionResult)NotFound() : Ok(alimentos);
            }
        }
        /// <summary>
        /// Método Get con la acción AlimentoDemandado para enviar datos al API
        /// </summary>
        /// <returns>Lista con los 5 alimentos mas demandados por cantidad</returns>
        [HttpGet]
        [Route("api/Alimento/AlimentoDemandado")]
        public IHttpActionResult AlimentoDemandado()
        {
            List<Alimento> listaAlimentos = _repositorio.DameListaAlimentos();
            List<Dieta> listaDietas = _repositorio.DameListaDietas();
            List<Comida> prueba = new List<Comida>();
            foreach (var item in listaAlimentos)
            {
                var listaSecundaria = (from alimento in listaDietas
                                       where alimento.AlimentoID == item.AlimentoID
                                       select alimento).Sum(x => x.Cantidad);
                if (listaSecundaria != null)
                {
                    var comida = new Comida(item.Descripcion, (int)listaSecundaria);
                    prueba.Add(comida);
                }
            }
            var listaFinal = (from comida in prueba
                              orderby comida.Cantidad descending
                              select comida).Take(5);
            return Ok(listaFinal.ToList());
        }

        public IHttpActionResult PostCreaAlimento([FromBody] Alimento alimento)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            return _repositorio.CreaAlimento(alimento) ? Ok(alimento) : (IHttpActionResult)NotFound();
        }
    }
}
