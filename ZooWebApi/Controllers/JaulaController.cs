﻿using Autofac;
using System.Collections.Generic;
using System.Web.Http;
using ZooWebApi.Models;
using ZooWebApi.Services.InyeccionD;
using ZooWebApi.Services.Repositories;

namespace ZooWebApi.Controllers
{
    
        /// <summary>
        /// Clase controlador para la clase Jaula
        /// </summary>
        public class JaulaController : ApiController
        {
            readonly IRepository _repositorio;

            /// <summary>
            /// Constructor vacio para que realice la inyeccion de dependencias de IRepository
            /// </summary>
            public JaulaController()
            {
                using (var scope = AutofacConfig.Container.BeginLifetimeScope())
                {
                    _repositorio = scope.Resolve<IRepository>();
                }
            }

            /// <summary>
            /// Contructor con parámetro Irepository para los Test
            /// </summary>
            /// <param name="repositorio">Repositorio Fake para pruebas</param>
            public JaulaController(IRepository repositorio)
            {
                _repositorio = repositorio;
            }

            /// <summary>
            /// Método Get para enviar los datos al API
            /// </summary>
            /// <param name="id">Id de Jaula con opción que si no se introduce se pasa nulo</param>
            /// <returns>Lista de Jaula sin parametro o 
            /// Información de la Jaula con el id igual al parámetro</returns>
            [System.Web.Mvc.HttpGet]
            public IHttpActionResult Get(int? id = null)
            {
                if (id == null)
                {
                    return Ok(_repositorio.DameListaJaulas());
                }
                else
                {
                    List<Jaula> jaulas = new List<Jaula>
                    {
                        _repositorio.DameJaula(id.Value)
                    };
                return jaulas[0] == null ? (IHttpActionResult)NotFound() : Ok(jaulas);
                }
            }
            public IHttpActionResult PostCreaJaula([FromBody] Jaula jaula)
            {
                if (!ModelState.IsValid)
                    return BadRequest("Invalid data.");

                return _repositorio.CreaJaula(jaula) ? Ok(jaula) : (IHttpActionResult)NotFound();
            }
    }
    }


