﻿using Autofac;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Results;
using ZooWebApi.Models;
using ZooWebApi.Services.InyeccionD;
using ZooWebApi.Services.Repositories;

namespace ZooWebApi.Controllers
{
    /// <summary>
    /// Clase controlador para la clase Animal
    /// </summary>
    public class AnimalController : ApiController
    {
        readonly IRepository _repositorio;
        /// <summary>
        /// Constructor vacio para que realice la inyeccion de dependencias de IRepository
        /// </summary>
        public AnimalController()
        {
            using (var scope = AutofacConfig.Container.BeginLifetimeScope())
            {
                _repositorio = scope.Resolve<IRepository>();
            }
        }
        /// <summary>
        /// Contructor con parámetro Irepository para los Test
        /// </summary>
        /// <param name="repositorio">Repositorio Fake para pruebas</param>
        public AnimalController(IRepository repositorio)
        {
            _repositorio = repositorio;
        }
       
        /// <summary>
        /// Método Get para enviar los datos al API
        /// </summary>
        /// <param name="id">Id de Animal con opción que si no se introduce se pasa nulo</param>
        /// <returns>Lista de Animales sin parametro o 
        /// Información del Animal con el id igual al parámetro</returns>
        [HttpGet]
        public IHttpActionResult Get(int? id = null)
        {
            List<Animal> _animales;
            if (id == null)
            {
                return Ok(_repositorio.DameListaAnimales());
            }
            else
            {
                _animales = new List<Animal>
                {
                    _repositorio.DameAnimal(id.Value)
                };
                return _animales[0] == null ? (IHttpActionResult)NotFound() : Ok(_animales);
            }
        }
        /// <summary>
        /// Crea Animal tomando la información del body
        /// </summary>
        /// <param name="animal">Parámetros del animal</param>
        /// <returns>devuelve si la petición ha sido correcta</returns>
        public IHttpActionResult PostCreaAnimal([FromBody] Animal animal)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            return _repositorio.CreaAnimal(animal) ? Ok(animal) : (IHttpActionResult)NotFound();
        }
        /// <summary>
        /// Edita en los animales del repositorio
        /// </summary>
        /// <param name="animal"></param>
        /// <returns>devuelve si la petición ha sido correcta</returns>
        public IHttpActionResult PutEditaAnimal([FromBody] Animal animal)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid data.");

            var animalEditar = _repositorio.DameAnimal(animal.AnimalID);

            if (animalEditar != null)
            {
                _repositorio.EditaAnimal(animal);
                return Ok(animal);
            }
            else
            {
                return NotFound();
            }
        }
    }
}