﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooWebApi.Controllers;
using ZooWebApi.Services.Repositories;

namespace ZooWebApi.Tests
{
    [TestClass]
    public class EspecieControllerTest
    {
        readonly IRepository repositorio = new FakeRepository();
        [TestMethod]
        public void EspecieGetOk()
        {
            //Declaraciones
            var controller = new EspecieController(repositorio);

            // Resultado
            var actionResult = controller.Get(1);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Especie>>));
        }
        [TestMethod]
        public void EspecieGetFail()
        {
            //Declaraciones
            var controller = new EspecieController(repositorio);

            // Resultado
            var actionResult = controller.Get(9875158);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void EspecieGetAllOk()
        {
            //Declaraciones
            var controller = new EspecieController(repositorio);

            // Resultado
            var actionResult = controller.Get();

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Especie>>));
        }
        [TestMethod]
        public void EspeciePostOk()
        {
            //Declaraciones
            var controller = new EspecieController(repositorio);

            // Resultado
            var actionResult = controller.PostCreaEspecie(repositorio.DameEspecie(1));

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<Models.Especie>));
        }
        [TestMethod]
        public void EspeciePostFail()
        {
            //Declaraciones
            var controller = new EspecieController(repositorio);

            // Resultado
            var actionResult = controller.PostCreaEspecie(null);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
    }
}
