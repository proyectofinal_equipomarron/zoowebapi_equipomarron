﻿using System;
using System.Collections.Generic;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooWebApi.Controllers;
using ZooWebApi.Services.Repositories;

namespace ZooWebApi.Tests
{
    [TestClass]
    public class AnimalControllerTest
    {
        readonly IRepository repositorio = new FakeRepository();
        [TestMethod]
        public void AnimalGetOk()
        {
            //Declaraciones
            var controller = new AnimalController(repositorio);

            // Resultado
            var actionResult = controller.Get(1);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Animal>>));
        }
        [TestMethod]
        public void AnimalGetFail()
        {
            //Declaraciones
            var controller = new AnimalController(repositorio);

            // Resultado
            var actionResult = controller.Get(9875158);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void AnimalGetAllOk()
        {
            //Declaraciones
            var controller = new AnimalController(repositorio);

            // Resultado
            var actionResult = controller.Get();

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Animal>>));
        }
        [TestMethod]
        public void AnimalPostOk()
        {
            //Declaraciones
            var controller = new AnimalController(repositorio);

            // Resultado
            var actionResult = controller.PostCreaAnimal(repositorio.DameAnimal(1));

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<Models.Animal>));
        }
        [TestMethod]
        public void AnimalPostFail()
        {
            //Declaraciones
            var controller = new AnimalController(repositorio);

            // Resultado
            var actionResult = controller.PostCreaAnimal(null);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
    }
}
