﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooWebApi.Controllers;
using ZooWebApi.Services.Repositories;

namespace ZooWebApi.Tests
{
    [TestClass]
    public class JaulaControllerTest
    {
        readonly IRepository repositorio = new FakeRepository();
        [TestMethod]
        public void JaulaGetOk()
        {
            //Declaraciones
            var controller = new JaulaController(repositorio);

            // Resultado
            var actionResult = controller.Get(1);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Jaula>>));
        }
        [TestMethod]
        public void JaulaGetFail()
        {
            //Declaraciones
            var controller = new JaulaController(repositorio);

            // Resultado
            var actionResult = controller.Get(9875158);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void JaulaGetAllOk()
        {
            //Declaraciones
            var controller = new JaulaController(repositorio);

            // Resultado
            var actionResult = controller.Get();

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Jaula>>));
        }
        [TestMethod]
        public void JaulaPostOk()
        {
            //Declaraciones
            var controller = new JaulaController(repositorio);

            // Resultado
            var actionResult = controller.PostCreaJaula(repositorio.DameJaula(1));

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<Models.Jaula>));
        }
        [TestMethod]
        public void JaulaPostFail()
        {
            //Declaraciones
            var controller = new JaulaController(repositorio);

            // Resultado
            var actionResult = controller.PostCreaJaula(null);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
    }
}
