﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZooWebApi.Controllers;
using ZooWebApi.Services.Repositories;

namespace ZooWebApi.Tests
{
    [TestClass]
    public class DietaControllerTest
    {
        readonly IRepository repositorio = new FakeRepository();
        [TestMethod]
        public void DietaGetOk()
        {
            //Declaraciones
            var controller = new DietaController(repositorio);

            // Resultado
            var actionResult = controller.Get(1);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Dieta>>));
        }
        [TestMethod]
        public void DietaGetFail()
        {
            //Declaraciones
            var controller = new DietaController(repositorio);

            // Resultado
            var actionResult = controller.Get(9875158);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void DietaGetAllOk()
        {
            //Declaraciones
            var controller = new DietaController(repositorio);

            // Resultado
            var actionResult = controller.Get();

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Dieta>>));
        }
        [TestMethod]
        public void DietaPostOk()
        {
            //Declaraciones
            var controller = new DietaController(repositorio);

            // Resultado
            var actionResult = controller.PostCreaDieta(repositorio.DameDieta(1));

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<Models.Dieta>));
        }
        [TestMethod]
        public void DietaPostFail()
        {
            //Declaraciones
            var controller = new DietaController(repositorio);

            // Resultado
            var actionResult = controller.PostCreaDieta(null);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
    }
}
