﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web.Http.Results;
using ZooWebApi.Controllers;
using ZooWebApi.Services.Repositories;

namespace ZooWebApi.Tests
{
    [TestClass]
    public class AlimentoControllerTest
    {
        readonly IRepository repositorio = new FakeRepository();
        [TestMethod]
        public void AlimentoGetOk()
        {
            //Declaraciones
            var controller = new AlimentoController(repositorio);

            // Resultado
            var actionResult = controller.Get(1);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Alimento>>));
        }
        [TestMethod]
        public void AlimentoGetFail()
        {
            //Declaraciones
            var controller = new AlimentoController(repositorio);

            // Resultado
            var actionResult = controller.Get(9875158);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void AlimentoGetAllOk()
        {
            //Declaraciones
            var controller = new AlimentoController(repositorio);

            // Resultado
            var actionResult = controller.Get();

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Alimento>>));
        }
        [TestMethod]
        public void AlimentoPostOk()
        {
            //Declaraciones
            var controller = new AlimentoController(repositorio);

            // Resultado
            var actionResult = controller.PostCreaAlimento(repositorio.DameAlimento(1));

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<Models.Alimento>));
        }
        [TestMethod]
        public void AlimentoPostFail()
        {
            //Declaraciones
            var controller = new AlimentoController(repositorio);

            // Resultado
            var actionResult = controller.PostCreaAlimento(null);

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void AlimentoDemandadoGetOk()
        {
            //Declaraciones
            var controller = new AlimentoController(repositorio);

            // Resultado
            var actionResult = controller.AlimentoDemandado();

            // Verificación
            Assert.IsInstanceOfType(actionResult, typeof(OkNegotiatedContentResult<List<Models.Comida>>));
        }
    }
}
